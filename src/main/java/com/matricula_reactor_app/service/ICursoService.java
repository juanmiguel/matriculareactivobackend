package com.matricula_reactor_app.service;


import org.springframework.data.domain.Pageable;

import com.matricula_reactor_app.document.Curso;
import com.matricula_reactor_app.pagination.PageSupport;
import com.matricula_reactor_app.service.ICRUD;

import reactor.core.publisher.Mono;

public interface ICursoService extends ICRUD<Curso, String>{

	Mono<PageSupport<Curso>> listarPage(Pageable page);
}
