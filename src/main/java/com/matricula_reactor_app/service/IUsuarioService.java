package com.matricula_reactor_app.service;

import com.matricula_reactor_app.document.Usuario;
import com.matricula_reactor_app.security.User;

import reactor.core.publisher.Mono;

public interface IUsuarioService  extends ICRUD<Usuario, String>{

	Mono<User> buscarPorUsuario(String usuario);
}
