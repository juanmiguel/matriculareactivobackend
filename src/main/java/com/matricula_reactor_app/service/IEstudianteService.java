package com.matricula_reactor_app.service;

import org.springframework.data.domain.Pageable;

import com.matricula_reactor_app.document.Estudiante;
import com.matricula_reactor_app.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface IEstudianteService  extends ICRUD<Estudiante, String> {
	
	Flux<Estudiante> findByNombresNotNull();
	
	Mono<PageSupport<Estudiante>> listarPage(Pageable page);
}
