package com.matricula_reactor_app.service;

import org.springframework.data.domain.Pageable;

import com.matricula_reactor_app.document.Matricula;
import com.matricula_reactor_app.pagination.PageSupport;

import reactor.core.publisher.Mono;

public interface IMatriculaService  extends ICRUD<Matricula, String>{

	Mono<PageSupport<Matricula>> listarPage(Pageable page);
}
