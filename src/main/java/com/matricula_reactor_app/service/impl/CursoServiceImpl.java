package com.matricula_reactor_app.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.matricula_reactor_app.document.Curso;
import com.matricula_reactor_app.pagination.PageSupport;
import com.matricula_reactor_app.repo.ICursoRepo;
import com.matricula_reactor_app.service.ICursoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CursoServiceImpl implements ICursoService {

	@Autowired
	private ICursoRepo repo;
	
	@Override
	public Mono<Curso> registrar(Curso t) {
		return repo.save(t);
	}

	@Override
	public Mono<Curso> modificar(Curso t) {
		return repo.save(t);
	}

	@Override
	public Flux<Curso> listar() {
		return repo.findAll();
	}

	@Override
	public Mono<Curso> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

	@Override
	public Mono<PageSupport<Curso>> listarPage(Pageable page) {
	    return repo.findAll()
		        .collectList()
		        .map(list -> new PageSupport<>(
		            list
		                .stream()
		                .skip(page.getPageNumber() * page.getPageSize())
		                .limit(page.getPageSize())
		                .collect(Collectors.toList()),
		            page.getPageNumber(), page.getPageSize(), list.size()));
	}

}
