package com.matricula_reactor_app.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.matricula_reactor_app.document.Matricula;
import com.matricula_reactor_app.pagination.PageSupport;
import com.matricula_reactor_app.repo.IMatriculaRepo;
import com.matricula_reactor_app.service.IMatriculaService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MatriculaServiceImpl implements IMatriculaService {

	@Autowired
	private IMatriculaRepo repo;

	@Override
	public Mono<Matricula> registrar(Matricula t) {
		return repo.save(t);	}

	@Override
	public Mono<Matricula> modificar(Matricula t) {
		return repo.save(t);
	}

	@Override
	public Flux<Matricula> listar() {
		return repo.findAll();
	}

	@Override
	public Mono<Matricula> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

	@Override
	public Mono<PageSupport<Matricula>> listarPage(Pageable page) {
	    return repo.findAll()
		        .collectList()
		        .map(list -> new PageSupport<>(
		            list
		                .stream()
		                .skip(page.getPageNumber() * page.getPageSize())
		                .limit(page.getPageSize())
		                .collect(Collectors.toList()),
		            page.getPageNumber(), page.getPageSize(), list.size()));
	}
	
}
