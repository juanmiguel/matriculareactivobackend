package com.matricula_reactor_app.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.matricula_reactor_app.document.Estudiante;
import com.matricula_reactor_app.pagination.PageSupport;
import com.matricula_reactor_app.repo.IEstudianteRepo;
import com.matricula_reactor_app.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EstudianteServiceImpl implements IEstudianteService {

	@Autowired
	private IEstudianteRepo repo;
	
	@Override
	public Mono<Estudiante> registrar(Estudiante t) {
		return repo.save(t);	}

	@Override
	public Mono<Estudiante> modificar(Estudiante t) {
		return repo.save(t);	}

	@Override
	public Flux<Estudiante> listar() {
		return repo.findAll();	}

	@Override
	public Mono<Estudiante> listarPorId(String v) {
		return repo.findById(v);	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

	@Override
	public Flux<Estudiante> findByNombresNotNull() {
		return repo.findByNombresNotNull();
	}

	@Override
	public Mono<PageSupport<Estudiante>> listarPage(Pageable page) {
	    return repo.findAll()
		        .collectList()
		        .map(list -> new PageSupport<>(
		            list
		                .stream()
		                .skip(page.getPageNumber() * page.getPageSize())
		                .limit(page.getPageSize())
		                .collect(Collectors.toList()),
		            page.getPageNumber(), page.getPageSize(), list.size()));
	}



}
