package com.matricula_reactor_app.document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cursos")
public class Curso {

	@Id
	private String id;

	@NotEmpty(message = "No puede ser vacío")
	@Size(min = 5, max = 15, message="Debe ser entre 5 y 15 caracteres")	
	private String nombre;

	@NotEmpty(message = "No puede ser vacío")
	@Size(min = 1, max = 10, message="Debe ser entre 5 y 10 caracteres")
	private String siglas;

	@NotNull(message = "No puede ser nulo")
	private boolean estado;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSiglas() {
		return siglas;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
}
