package com.matricula_reactor_app.document;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModelProperty;

@Document(collection = "estudiantes")
public class Estudiante {

	@Id
	private String id;

	@NotEmpty(message = "No puede ser vacío")
	@Size(min = 5, max = 15, message="Debe ser entre 5 y 15 caracteres")
	private String nombres;

	@NotEmpty(message = "No puede ser vacío")
	@Size(min = 5, max = 15, message="Debe ser entre 5 y 15 caracteres")
	private String apellidos;	
	
	
	@Size(min = 8, max = 8, message="El dni debe tener 8 dígitos")
	private String dni;

	
	@ApiModelProperty(value = "Valor minimo 1, Valor maximo 100")
	@NotNull
	@Max(100)
	@Min(1)
	private double edad;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public double getEdad() {
		return edad;
	}

	public void setEdad(double edad) {
		this.edad = edad;
	}
	
	
}
