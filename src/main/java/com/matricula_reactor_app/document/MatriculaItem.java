package com.matricula_reactor_app.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "matriculas_items")
public class MatriculaItem {

	@Id
	private String id;
	@DBRef
	private Curso curso;
	
	public MatriculaItem() {
	}

	public MatriculaItem(String id, Integer cantidad, Curso curso) {
		super();
		this.id = id;
		this.curso = curso;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
}
