package com.matricula_reactor_app.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.matricula_reactor_app.document.Estudiante;
import com.matricula_reactor_app.pagination.PageSupport;
import com.matricula_reactor_app.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

	@Autowired
	private IEstudianteService service;

	//Listar todos
	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>> listar(){
		Flux<Estudiante> Flux = service.listar();		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_STREAM_JSON)
				.body(Flux));
	}

	//Lista los estudiantes en orden descendente por edad paralelo
	@GetMapping("/paralleldesc")	
	public Flux<Estudiante> listarparalleldesc(){
		Flux<Estudiante> estudiantesFlux = WebClient.create("http://localhost:8080/estudiantes")
				.get()
				.retrieve()
				.bodyToFlux(Estudiante.class);

		return estudiantesFlux
				.parallel()
				.runOn(Schedulers.elastic())
				.flatMap(p -> service.listarPorId(p.getId() ))
				.ordered((p1, p2) -> (int)p2.getEdad() - (int)p1.getEdad());		
	
	}
	
	//Listar los estudiantes en orden descendente por edad
	@GetMapping("/desc")
	public Mono<ResponseEntity<Flux<Estudiante>>> listardesc(){
		Flux<Estudiante> Flux = service.findByNombresNotNull();
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_STREAM_JSON)
				.body(Flux));
	}
	
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id){
		return service.listarPorId(id)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p)
				).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest req){
		return service.registrar(estudiante)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p)
					).onErrorMap(error -> new ArithmeticException("Error"));
	}
	
	@PutMapping
	public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante){
		return service.modificar(estudiante)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p)
					).defaultIfEmpty(ResponseEntity.notFound().build());	
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id){
		return service.listarPorId(id)
				.flatMap(p -> {
					return service.eliminar(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@GetMapping("/hateoas/{id}")
	public Mono<EntityModel<Estudiante>> listarHateoasPorId(@PathVariable("id") String id) {
		//cursos/hateoas/1
		Mono<Link> selfLink = linkTo(methodOn(EstudianteController.class).listarHateoasPorId(id)).withSelfRel().toMono();		
		Mono<Link> selfLink2 = linkTo(methodOn(EstudianteController.class).listarHateoasPorId(id)).withSelfRel().toMono();
		return selfLink.zipWith(selfLink2)
				.map(function((izq, der) -> Links.of(izq, der)))
				.zipWith(service.listarPorId(id), (links, p) -> {
					return new EntityModel<>(p, links);
				});
	}
	
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPageable(
			@RequestParam(name = "page", defaultValue="0") int page,
			@RequestParam(name = "size", defaultValue="10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p)
				).defaultIfEmpty(ResponseEntity.notFound().build());
		
	}

}
