package com.matricula_reactor_app.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.matricula_reactor_app.document.Matricula;


public interface IMatriculaRepo  extends ReactiveMongoRepository<Matricula, String> {

}
