package com.matricula_reactor_app.repo;


import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.matricula_reactor_app.document.Estudiante;

import reactor.core.publisher.Flux;

public interface IEstudianteRepo extends ReactiveMongoRepository<Estudiante, String>{

	  @Query(sort = "{ edad : -1 }")
	  Flux<Estudiante> findByNombresNotNull(); 
	  
}
