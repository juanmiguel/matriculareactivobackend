package com.matricula_reactor_app.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.matricula_reactor_app.document.Usuario;

import reactor.core.publisher.Mono;

public interface IUsuarioRepo extends ReactiveMongoRepository<Usuario, String>{

	 Mono<Usuario> findOneByUsuario(String usuario);

}
