package com.matricula_reactor_app.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.matricula_reactor_app.document.Curso;

public interface ICursoRepo extends ReactiveMongoRepository<Curso, String>{

}
