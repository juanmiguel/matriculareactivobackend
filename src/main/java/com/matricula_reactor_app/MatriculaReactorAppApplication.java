package com.matricula_reactor_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatriculaReactorAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatriculaReactorAppApplication.class, args);
	}

}
