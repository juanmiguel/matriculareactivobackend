# Api Rest de Matrículas

_El proyecto ha sido creado con Java y Mongo, está basado en el curso de spring boot reactivo._

## Detalle 🚀

_El proyecto cuenta con una capa de seguridad que permite el acceso a las rutas por medio de tokens._

### Pre-requisitos 📋

_El proyecto se creó con las siguientes herramientas._

```
STS
Mongo
Postman
Robo 3T
```

---
